<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    
   public function shirts() {
       return $this->belongsToMany('App\Shirt')
               ->withPivot('quantity');
   }
    
}
