<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    
    // non standard table name
    // public $table = ''; 
    
    // this table does not use timestamps
    public $timestamps = false;
    
    // set primary key name
    public $primaryKey = 'customerNumber';
    
    
    // format to be used on object (not statically)
    public function getSpainWithGoodCredit() {
        return $this->query()
                ->where('creditLimit', '>=', 100000)
                ->where('country', '=', 'Spain')
                ->get();
    }
    
    
    // format to be used on class (statically)
    public static function getSpainWithGoodCreditStatic() {
        return self::
                where('creditLimit', '>=', 100000)
                ->where('country', '=', 'Spain')
                ->get();
    }
    
    
    // scope to spanish customers
    public function scopeSpanish($query) {
        return $query->where('country', '=', 'Spain');
    }
    
    
    // scope to crdit over 100000
    public function scopeHighCredit($query) {
        return $query->where('creditLimit', '>=', 100000);
    }
    
    
    // each customer has many orders
    public function orders() {
        return $this->hasMany('App\Order', 'customerNumber');
    }
    
    
}
