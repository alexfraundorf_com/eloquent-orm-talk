<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    
    // non standard table name
    // public $table = ''; 
    
    // this table does not use timestamps
    public $timestamps = false;
    
    // set primary key name
    public $primaryKey = 'orderNumber';
    

    // orders that belong to a customer
    public function customer() {
        return $this->belongsTo('App\Customer', 'customerNumber');
    }
    
    
}
