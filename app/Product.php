<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    // non standard table name
    // public $table = ''; 
    
    // this table does not use timestamps
    public $timestamps = false;
    
    // set primary key name
    public $primaryKey = 'productCode';
    
   
    
}
