<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Database\Schema\Blueprint;


Route::get('/', function () {
    return view('welcome');
});


Route::get('create_test_table', function() {
    // create the table
    Schema::create('test', function(Blueprint $table) {
        // create the primary key
        $table->increments('id');
        // create some columns
        $table->string('name');
        $table->integer('count');
        $table->decimal('price');
        $table->text('comments');
        $table->timestamps();
    });
});


Route::get('create-customer', function() {
    // create a new customer record
    $customer = new \App\Customer;
    $customer->customerNumber = 500; // this table does not auto increment
    $customer->customerName = 'New Customer';
    $customer->contactLastName = 'Doe';
    $customer->contactFirstName = 'John';
    $customer->phone = '555-1234';
    $customer->addressLine1 = '123 Fake Street';
    $customer->city = 'Appleton';
    $customer->state = 'WI';
    $customer->postalCode = '54915';
    $customer->country = 'USA';
    $customer->save();
    echo 'The new customer id is ' . $customer->customerNumber;
});


Route::get('get-all-customers', function() {
    return \App\Customer::all();
});


Route::get('get-customer-by-id', function() {
    $id = 121;
    //$id = 9000; // doesn't exist
    return \App\Customer::find($id);
});


Route::get('get-customer-where1', function() {
    return \App\Customer::where('country', 'LIKE', 'Fra%')
            ->where('city', '=', 'Paris')
            ->orderBy('addressLine1', 'DESC')
            ->first();
});


Route::get('update-customer', function() {
    // get the customer we want to update
    $id = 121;
    $customer = \App\Customer::find($id);
    //return $customer;
    // update the customer's data
    $customer->city = 'Bananarama';
    $customer->country = 'Lichtenstein';
    $customer->save();
    return $customer;
});


Route::get('update-multiple', function() {
    // target the rows we want to update
    \App\Customer::where('country', '=', 'Germany')
            // update fields in these rows
            ->update([
                // fields and values to update
                'state' => 'Bavaria',
                'country' => 'Deutschland',                
            ]);
    // retrieve and return records that were updated
    return \App\Customer::where('country', '=', 'Deutschland')->get();
});


Route::get('delete-customer', function() {
    // get the customer we want to delete
    $id = 125;
    $customer = \App\Customer::find($id)->delete();
});


Route::get('delete-multiple1', function() {
    // find customers to delete
    \App\Customer::where('country', '=', 'Deutschland')
            ->where('salesRepEmployeeNumber', '=', null)
            // delete them
            ->delete();
});


Route::get('delete-multiple2', function() {
    // ids to delete
    $ids_to_delete = [465, 477, 480, 481];
    // delete (destroy)
    \App\Customer::destroy($ids_to_delete); 
});


Route::get('get-good-credit', function() {
    $customer = new \App\Customer();
    return $customer->getSpainWithGoodCredit();
});


Route::get('get-good-credit-static', function() {
    return \App\Customer::getSpainWithGoodCreditStatic();
});


Route::get('spanish-customers', function() {
    return \App\Customer::spanish()->get();
});


Route::get('high-credit-customers', function() {
    return \App\Customer::highCredit()->get();
});


Route::get('high-credit-spanish-customers', function() {
    return \App\Customer::highCredit()->spanish()->get();
});


Route::get('get-customer-orders', function() {
    $customer_name = 'Danish Wholesale Imports';
    return \App\Customer
            ::with('orders')
            ->where('customerName', 'LIKE', $customer_name)
            ->get();
});


Route::get('add-new-category-to-list', function() {
    $todo = \App\Todolist::find(1);
    $category_name = 'Vacation';
    $category = new \App\Category(['name' => $category_name]);
    $todo->categories()->save($category);
});


Route::get('attach-category', function() {
    $todo = \App\Todolist::find(1);
    $category = \App\Category::find(1);
    $todo->categories()->attach($category); // attach with object
    //$todo->categories()->attach(2); // attach by category id
    //$todo->categories()->attach([3,4]); // attach an array of ids
});

Route::get('detach-category', function() {
    $todo = \App\Todolist::find(1);
    $category = \App\Category::find(2);
    $todo->categories()->detach($category); // detach with object
    //$todo->categories()->detach(1); // detach by category id
    //$todo->categories()->detach([3,4]); // detach an array of ids
});